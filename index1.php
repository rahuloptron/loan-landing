﻿<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
<title>Loan Landing Page</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">

<!-- Favicon --> 
<link rel="shortcut icon" href="images/favicon.ico">

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>


<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- ######### CSS STYLES ######### -->
<link rel="stylesheet" href="css/reset-law.css" type="text/css" />
<link rel="stylesheet" href="css/style-law.css" type="text/css" />

<!-- font awesome icons -->
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />

<!-- et linefont icons -->
<link rel="stylesheet" href="css/et-linefont/etlinefont.css">

<!-- animations -->
<link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="css/responsive-leyouts-law.css" type="text/css" />

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="css/shortcodes-law.css" type="text/css" /> 

<!-- style switcher -->
<link rel = "stylesheet" media = "screen" href = "js/style-switcher/color-switcher.css" />

<!-- mega menu -->
<link href="js/mainmenu/bootstrap.min1.css" rel="stylesheet">
<link href="js/mainmenu/demo.css" rel="stylesheet">
<link href="js/mainmenu/menu-law.css" rel="stylesheet">

<!-- MasterSlider -->
<link rel="stylesheet" href="js/masterslider/style/masterslider1.css" />
<link rel="stylesheet" href="js/masterslider/skins/default/style-la.css" />

<!-- carouselowl -->
<link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
<link href="js/carouselowl/owl.carousel.css" rel="stylesheet">
<link href="js/carouselowl/owl.theme.css" rel="stylesheet">


</head>

<body>

<div class="site_wrapper">

<div class="top_header">
<div class="container">
    
    <div class="left">
    
        <!-- Logo -->
        <div><a href="index.html" id="logo"></a></div>
    
    </div><!-- end left -->
    
    <div class="right">
    
        (01) 123-456-7890
        <a href="mailto:info@hslawyer.com" class="email">info@loanbazaar.com</a>
       
    </div><!-- end right -->

</div>
</div><!-- end top navigation links -->

<div class="clearfix"></div>


<!-- Slider
======================================= --> 
<div class="feature_section4">
<div class="container">
<div class="content">
    
<h1>About</h1>
    <h2>Upstream Business Consultants LLP</h2>
    <h4>Upstream  Business Consultants LLP is a advisory firm that offers a full range of  retail financial services. We are a one stop service provider that offers facilities such as funding, housing loans, personal loans and retail loans. We have tie ups with 30 leading banks and NBFCs, that offer competitive interest rates. We offer our services with dedication, passion and skill.</h4>

</div>

</div>
</div>
<!-- end slider -->
  
  
<div class="clearfix"></div>
  
  
<div class="feature_section3">
<div  class="container">

    <h1>What is a housing loan?</h1> 

    <p class="less6">A Housing loan acquired from a financial institution to purchase a home loan.  Home loans may also be referred to as mortgage loans.Interest rates offered on home loans can be either fixed or floating.Fixed rate is one in which the same interest rate is applicable throughout the loan tenure.Floating rate is one in which the interest rate varies during the home loan tenure, as per changes in market rates.</p>

</div>
</div> <!-- end feature_section 1 -->
  
  
<div class="clearfix"></div>
  
  
<div class="feature_section2">
<div class="container">

    
    <h1>What is a personal Loan?</h1>
   
    <p class="less6">A personal loan is a type of unsecured loan and helps to meet one’s current financial needs. One usually doesn’t need to pledge any security or collateral while availing of a personal loan and the lender provides the flexibility to use the funds as per one’s need. It can serve as a solution for funding holidays and wedding expenses,  as well as medical emergencies, home renovation, debt consolidation and others.</p>

</div>
</div><!-- end feature_section 3 -->
  

<div class="clearfix"></div>
  
  
<div class="feature_section1">
<div class="container">

    <h2>Why Us?</h2>

   <ul>
       
<li> Tie Up with 30 leading banks and NBFCS</li>
<li> Home Loans / Personal Loans / Business Loans</li>
<li>Doorstep Service and A Dedicated Relationship Manager</li>
<li>Selected as top 30 most fundable start ups in India for 2018</li>
<li>Leadership team comprises of Senior Ex Bankers</li>
<li>Please visit our facebook page: <a href="https://www.facebook.com/upstreambusinessconsultants/">https://bit.ly/2HEAJ92</a></li>
<li>EMI Calculator : <a href="https://emicalculator.net/">https://emicalculator.net/</a></li>

   </ul>

</div>
</div>
  
  
<div class="clearfix"></div>

<div class="feature_section8">
<div class="container">

    <h2>Contact Us</h2>
    <div class="clearfix margin_bottom1"></div>
  
    <div class="contact-form">
    <form action="index.html" method="post" id="yourid">
      <div class="box1">
        <label>
          <input type="text" name="samplees" id="samplees" value="Name" onFocus="if(this.value == 'Name') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Name';}" class="input">
        </label>
        <label>
          <input type="text" name="samplees" id="samplees2" value="Phone" onFocus="if(this.value == 'Phone') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Phone';}" class="input">
        </label>
      </div>
      <div class="box2">
        <label>
          <input type="text" name="samplees" id="samplees3" value="E-mail" onFocus="if(this.value == 'E-mail') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'E-mail';}" class="input">
        </label>
        <label>
                <select name="cars" class="select">
                <option value="select">Select Profession</option>
                <option value="Salaried">Salaried</option>
                <option value="Self Employed">Self Employed</option>
                </select>
        </label>
      </div>
      <div class="box1">
        <label>
          <input type="text" name="samplees" id="samplees" value="Net Monthly Income" onFocus="if(this.value == 'Net Monthly Income') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Net Monthly Income';}" class="input">
        </label>
        <label>
          <select name="cars" class="select">
                <option value="select">Any existing Loans</option>
                <option value="Yes">Yes</option>
                 <option value="No">No</option>
                </select>
        </label>
      </div>
      <div class="box2">
        <label>
          <select name="cars" class="select">
                <option value="select">Type of Loan Required</option>
                <option value="Home Loan">Home Loan</option>
                <option value="Personal Loan">Personal Loan</option>
                 <option value="Business Loan">Business Loan</option>
                </select>
        </label>
        <label>
          <input type="text" name="samplees" id="samplees4" value="Amount (Rs)    /   EMI (Rs)" onFocus="if(this.value == 'Amount (Rs)    /   EMI (Rs)') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Amount (Rs)    /   EMI (Rs)';}" class="input">
        </label>
      </div>
      <textarea rows="4" name="message" id="message" placeholder="Message"></textarea> <div class="clearfix"></div><br>
      <input type="checkbox" name="checkbox" value="check" id="agree" /><p class="checkboxtext"> I authorize LoanBazaar.com to call or SMS me in connection with my application & agree to the Privacy Policy and Terms of use
       </p>
        <div class="clearfix margin_top2"></div>
      <input name="yourname" value="Apply Now" class="button_send" type="submit" />
      
    </form>
    <div class="clearfix margin_top2"></div>
    </div>
  
</div>
</div>
<!-- end feature_section 8 -->

<div class="clearfix"></div>




<div class="clearfix"></div>

<div class="feature_section9">
<div class="container">

    <div class="box1"> <i class="fa fa-mobile-phone"></i>
    <h4 class="caps">Phone Number<b>+1 123 456 7890</b></h4></div>
    
    <div class="box2"> <i class="fa fa-envelope-o"></i>
    <h4 class="caps">Email Address<b><a href="#">info@loanbazaar.com</a></b></h4></div>
    
    <div class="box3"> <i class="fa fa-map-marker"></i>
    <h4 class="caps">Location Address<b>2901 MG Road, Glassgow, Seattle, WA.</b></h4></div>
    
    <div class="box4">
    Copyright © 2016 loanbazaar.com All rights reserved. <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
    </div>

</div>    
</div>
<!-- end feature_section 9 -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->
</div>

<!-- ######### JS FILES ######### --> 
<!-- get jQuery used for the theme --> 
<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/style-switcher/styleselector.js"></script>
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>

<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";
    var slider = new MasterSlider();
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('bullets');

    slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:620,   // slider standard height
         space:0,
         speed:45,
         layout:'fullwidth',
         loop:true,
         preload:0,
         autoplay:true,
         view:"parallaxMask"
    });
})(jQuery);
</script>

<!-- aninum --> 
<script src="js/aninum/jquery.animateNumber.min.js"></script>

<!-- carouselowl --> 
<script src="js/carouselowl/owl.carousel.js"></script> 

<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<script type="text/javascript" src="js/universal/custom.js"></script>

</body>
</html>
